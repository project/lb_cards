<?php

/**
 * @file
 * Primary module hooks for LB Cards module.
 */

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function lb_cards_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the lb_cards module.
    case 'help.page.lb_cards':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Cards for Layout Builder') . '</p>';
      return $output;
  }
}

/**
 * Implements hook_theme().
 */
function lb_cards_theme($existing, $type, $theme, $path) {
  return [
    'block__lb_cards' => [
      'base hook' => 'block',
      'template' => 'block--lb-cards',
    ],
    'block__lb_cards__variation_standard' => [
      'base hook' => 'block',
      'template' => 'block--lb-cards--variation-standard',
    ],
    'block__lb_cards__variation_overlay' => [
      'base hook' => 'block',
      'template' => 'block--lb-cards--variation-overlay',
    ],
    'block__lb_cards__variation_chevron' => [
      'base hook' => 'block',
      'template' => 'block--lb-cards--variation-chevron',
    ],
    'block__lb_cards__variation_color' => [
      'base hook' => 'block',
      'template' => 'block--lb-cards--variation-color',
    ],
  ];
}

/**
 * Implements hook_preprocess_block().
 */
function lb_cards_preprocess_block__lb_cards(&$variables) {
  $variables['block_content'] = $variables['elements']['content']['#block_content'];
  $variables['items'] = $variables['block_content']->field_block_item->referencedEntities();
  $columns = (int)$variables['block_content']->field_columns->value ?? 1;
  if ($columns === 0) {
    $columns = 1;
  }
  $variables['columns'] = $columns;
  $variables['row_class'] = "row-cols-lg-{$columns}";
  if ($columns >=3) {
    $variables['row_class'] .= ' row-cols-md-2';
  }
  else {
    $variables['row_class'] .= " row-cols-md-{$columns}";
  }

  // Add the current language so we can translate tags.
  $language = \Drupal::languageManager()->getCurrentLanguage();
  $variables['lang_code'] = $language->getId();
}

/**
 * Implements hook_ENTITY_TYPE_presave() for block entities.
 */
function lb_cards_block_content_presave(EntityInterface $block) {
  if ($block->bundle() === 'card_item') {
    $block->setInfo($block->get('field_title')->value);
  }
}

/**
 * Implements hook_inline_entity_form_entity_form_alter().
 */
function lb_cards_inline_entity_form_entity_form_alter(array &$entity_form, FormStateInterface $form_state) {
  $entity = $entity_form['#entity'];
  if ($entity->bundle() === 'card_item' && !empty($form_state->getUserInput()['settings']['block_form']['field_block_item']['form'])) {
    $form = $form_state->getUserInput()['settings']['block_form']['field_block_item']['form'];
    if (isset($form['inline_entity_form'])) {
      $resultForm = reset($form['inline_entity_form']['entities'])['form'];
    }
    else {
      $resultForm = reset($form);
    }
    $entity->setInfo($resultForm['field_title']['0']['value']);
  }
}
